<?php

/*
 * @file
 * Adds the Twitter Tweet Button to node teasers & pages
 */

/**
 * Settings page
 */
function tweet_button_admin() {
  /*
   * add some admin specific css/js & variables for the js
   */
  $path = drupal_get_path('module', 'tweet_button');
  drupal_add_js(array('tweet_button_path' => base_path() . $path), 'setting');
  drupal_add_js($path .'/js/admin.js');
  drupal_add_css($path . '/css/admin.css');
  
  /*
   * setup default options
   */
  $style = array(
    'vertical' => t('Vertical Count'),
    'horizontal' => t('Horizontal Count'),
    'no-count' => t('No Count'),
    'text' => t('Text'),
    'none' => t('None')
    );
  
  /*
   * Display Settings
   * TODO: allow custom text to be entered for 'Text' style
   */
  $form['display'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Display settings'),
    '#description'   => t('Setup how your Tweet Button will be displayed.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE
  );
  
  $form['display']['tweet_button_node'] = array(
    '#type' => 'select',
    '#title' => t('Display style on %what', array('%what' => 'nodes')),
    '#default_value' => SETTING_NODE,
    '#options' => $style,
    '#attributes' => array('class' => 'preview'),
    '#suffix' => '<div id="tweet-button-node-preview" class="preview">' . _tweet_button_preview(SETTING_NODE) . '</div>'
  );
  $form['display']['tweet_button_teaser'] = array(
    '#type' => 'select',
    '#title' => t('Display style on %what', array('%what' => 'teasers')),
    '#default_value' => SETTING_TEASER,
    '#options' => $style,
    '#attributes' => array('class' => 'preview'),
    '#suffix' => '<div id="tweet-button-teaser-preview" class="preview">' . _tweet_button_preview(SETTING_TEASER) . '</div>'
  );
  $form['display']['tweet_button_types'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Node types on which to display link'),
    '#description' => t('If no types are selected, the button will appear on all node types. To stop buttons from appearing on all types, choose %setting in the node and teaser display options above.', array('%setting' => 'None')),
    '#default_value' => variable_get('tweet_button_types', array()),
    '#options' => _tweet_button_node_types(),
  );
  
  /*
   * Recommend people to follow
   */
  $form['recommend'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Recommend people to follow'),
    '#description'   => t('Recommend up to two Twitter accounts for users to follow after they share content from your website. These accounts could include your own, or that of a contributor or a partner.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE
  );
  
  $form['recommend']['tweet_button_via'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('tweet_button_via', ''),
    '#description' => t('This user will be @ mentioned in the suggested Tweet')
  );
  $form['recommend']['tweet_button_related'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('tweet_button_related', ''),
    '#description' => t('Related account')
  );
  $form['recommend']['tweet_button_related_descr'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('tweet_button_related_descr', ''),
    '#description' => t('Related account description')
  );
  
  return system_settings_form($form);
}
================================================================================
Twitter Tweet Button Module
http://drupal.org/project/tweet_button
================================================================================

Adds the Twitter Tweet Button to node teasers & pages.

================================================================================
Installation
================================================================================

1. Download and unpack the module to the modules directory
2. Enable 'Twitter Tweet Button' @ 'admin/build/modules'
3. Configure settings @ 'admin/settings/tweet_button'

================================================================================
MORE INFORMATION
================================================================================

http://blog.twitter.com/2010/08/pushing-our-tweet-button.html
http://twitter.com/goodies/tweetbutton

================================================================================
Credits
================================================================================

Twitter Tweet Button contributed by @jsfwd / ninjagirl (http://ninjagirl.com)
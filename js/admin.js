/**
 * Tweet Button Admin
 */

if (Drupal.jsEnabled) {
  $(document).ready(function(){
    
    /* move the image preview divs inside the form item */
    $('#tweet-button-admin select.preview').each(function() {
      $(this).parent().append($(this).parent().next());
    });
    
    /* preview the selected button style */
    $('#tweet-button-admin select.preview').change(function() {
      var val = $(this).val();
      if (val == 'text' || val == 'none') {
        var img = '';
      } else {
        var img = '<img src="' + Drupal.settings.tweet_button_path + '/images/button_' + val + '.png" alt="Tweet Button"/>'; 
      }
      $(this).next().html(img);
    });
    
  });
}